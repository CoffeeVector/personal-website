from flask import Flask
from flask_assets import Environment, Bundle

from flask_compress import Compress

app = Flask(__name__)
Compress(app)

# assets
assets = Environment(app)

scss = Bundle(
    '../assets/css/styles.scss',
    '../assets/css/jumbotron.css',
    '../assets/css/cm-font.css',
    filters=['cssmin', 'libsass', 'jinja2'], output='gen/styles.min.css'
)
assets.register('scss_all', scss)

js = Bundle('../node_modules/bootstrap/dist/js/bootstrap.min.js', filters='jsmin', output='gen/packed.js')
assets.register('js_all', js)

# routes
from app import routes

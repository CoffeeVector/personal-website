from flask import render_template
from flask import redirect
import json
from app import app

@app.route('/')
def index():
    return render_template('index.jinja.html')

@app.route('/Resume.pdf')
def resume_pdf():
    return redirect('https://gitlab.com/CoffeeVector/resume/-/jobs/artifacts/master/raw/build/Resume.pdf?job=pdf')

@app.route('/Resume.jpg')
def resume_jpg():
    return redirect('https://gitlab.com/CoffeeVector/resume/-/jobs/artifacts/master/raw/build/Resume.jpg?job=jpg')

@app.route('/resume')
def resume():
    cse_coursework = [
        ('CSE 015', 'Discrete Mathematics'),
        ('CSE 030', 'Data Structures'),
        ('CSE 031', 'Computer Organization and Assembly Language'),
        ('ENGR 065', 'Circuit Theory'),
        ('CSE 100', 'Algorithms Analysis and Design'),
        ('CSE 120', 'Software Engineering'),
        ('CSE 150', 'Operating Systems'),
        ('CSE 165', 'Object Oriented Programming'),
        ('CSE 185', 'Computer Vision'),
        ('CSE 106', 'Full Stack Development'),
    ]
    phys_coursework = [
        ('PHYS 126', 'Special Relativity'),
        ('PHYS 108', 'Thermal Physics'),
        ('PHYS 105', 'Analytic Mechanics'),
        ('PHYS 112', 'Statistical Mechanics'),
        ('PHYS 160', 'Modern Physics Lab'),
        ('PHYS 110', 'Electrodynamics'),
        ('PHYS 115', 'Electrodynamics II'),
        ('PHYS 137', 'Quantum Mechanics'),
        ('PHYS 138', 'Quantum Mechanics II'),
        ('PHYS 180', 'Nonlinear Dynamics'),
        ('PHYS 192', 'Advanced Quantum Computing'),
        ('PHYS 116', 'Mathematical Methods'),
    ]
    return render_template('resume.jinja.html',
        cse_coursework  = cse_coursework,
        phys_coursework = phys_coursework,
    )

@app.route('/research')
def research():
    return render_template('research.jinja.html')

@app.route('/dnd')
def dnd():
    return redirect('https://docs.google.com/document/d/1FhKKXoTiEkeNGp8G-ZVThGYEfOOOZPc8UnKKsMNGM8c/edit?usp=sharing')
